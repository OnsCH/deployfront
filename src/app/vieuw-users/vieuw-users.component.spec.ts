import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VieuwUsersComponent } from './vieuw-users.component';

describe('VieuwUsersComponent', () => {
  let component: VieuwUsersComponent;
  let fixture: ComponentFixture<VieuwUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VieuwUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VieuwUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
