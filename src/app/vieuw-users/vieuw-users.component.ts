import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { user } from 'app/common/data/user';
import { UserService } from 'app/common/service/user.service';

@Component({
  selector: 'app-vieuw-users',
  templateUrl: './vieuw-users.component.html',
  styleUrls: ['./vieuw-users.component.css']
})
export class VieuwUsersComponent implements OnInit {

  allUsers: user[];
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.getAllUsers();
  }
  getAllUsers() {
    this.userService.getAllUsers().subscribe((data) => {
      console.log(data);
      this.allUsers = data;
    });
  }
  deleteUser(id: number) {
    this.userService.deleteUser(id).subscribe();
    this.router.navigate(['/view-profile']);
    
    
  }
  editUser(userr: user) {
    this.userService.setter(userr);
   
  }
  cancel() {
    this.router.navigate(['/']);
  }
}
