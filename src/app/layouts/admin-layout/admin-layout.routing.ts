import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import {LieuxAutourComponent} from '../../lieux-autour/lieux-autour.component';
import { UserRegisterComponent } from '../../user-register/user-register.component';
import { VieuwUsersComponent } from '../../vieuw-users/vieuw-users.component';
import { LoginComponent } from '../../login/login.component';

export const AdminLayoutRoutes: Routes = [
   
    { path: 'poissonsfavoris',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'table-list',     component: TableListComponent },
    { path: 'Acceuil',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'notifications',  component: NotificationsComponent },
    { path: 'lieuxAutour',        component: LieuxAutourComponent},
    { path: 'login',        component: LoginComponent},
    { path: 'register-profile',  component: UserRegisterComponent },
    { path: 'view-profile',  component: VieuwUsersComponent },
    { path: 'upgrade',        component: UpgradeComponent },
   

    

    
];
