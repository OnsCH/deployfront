import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from 'app/common/service/authorization.service';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/Acceuil', title: 'Bienvenue',  icon:'library_books', class: '' },

    { path: '/poissonsfavoris', title: 'Mon poisson favoris',  icon: 'fish', class: '' },
    { path: '/user-profile', title: 'User Profile',  icon:'person', class: '' },
   //{ path: '/table-list', title: 'Table List',  icon:'content_paste', class: '' },
  //  { path: '/login', title:'Login',icon: 'location_on' , class:''},

  //  { path: '/icons', title: 'Icons',  icon:'bubble_chart', class: '' },
    { path: '/maps', title: 'Maps',  icon:'location_on', class: '' },
  //  { path: '/notifications', title: 'Notifications',  icon:'notifications', class: '' },
    { path: '/lieuxAutour', title:'LieuxAutour',icon: 'location_on' , class:''},
    { path: '/register-profile', title: 'Register Profile',  icon:'person', class: '' },
    { path: '/view-profile', title: 'ViewUsers',  icon:'dashboard', class: '' },

  


];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(public _auth: AuthorizationService) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
