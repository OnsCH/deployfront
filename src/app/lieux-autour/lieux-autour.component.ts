import { Component, OnInit,ViewChild  } from '@angular/core';
import { MapInfoWindow, MapMarker, GoogleMap } from '@angular/google-maps';
import { couldStartTrivia } from 'typescript';
import { LieuxObject } from '.././common/data/lieux';
import { LieuxService } from '.././common/service/lieux.service';


@Component({
  selector: 'app-lieux-autour',
  templateUrl: './lieux-autour.component.html',
  styleUrls: ['./lieux-autour.component.css']
})
export class LieuxAutourComponent implements OnInit {

  @ViewChild(GoogleMap, { static: false }) map: GoogleMap
  @ViewChild(MapInfoWindow, { static: false }) info: MapInfoWindow
  nearLocations: LieuxObject[];
  distance: number;
  distanceXMaxi: number;
  distanceXMini: number;
  distanceYMaxi: number;
  distanceYMini: number;

i: number;
distanceXCalcul:number;
distanceYCalcul:number;

  zoom = 12;
  center: google.maps.LatLngLiteral
  options: google.maps.MapOptions = {
    zoomControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    mapTypeId: 'hybrid',
    maxZoom: 1000,
    minZoom: 2,
  }
  markers = []
  infoContent = ''
  constructor(private liService: LieuxService) { }
  ngOnInit() {
    navigator.geolocation.getCurrentPosition((position) => {
      this.center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
        
      }
     
    })
  
    this.i = 0;
    
   // this.convertDistanceEnLatLong();

  }

  zoomIn() {
    if (this.zoom < this.options.maxZoom) this.zoom++
  }

  zoomOut() {
    if (this.zoom > this.options.minZoom) this.zoom--
  }

  click(event: google.maps.MouseEvent) {
    console.log(event)
  }

  logCenter() {
    console.log(JSON.stringify(this.map.getCenter()));
    console.log(this.center.lat);
    console.log(this.center.lng);
   this.distanceXCalcul = this.distance/(111*(Math.cos(this.center.lat)));
   this.distanceYCalcul = this.distance/111;

    this.distanceXMaxi=this.center.lng + this.distanceXCalcul;
    this.distanceXMini= this.center.lng - this.distanceXCalcul;
    this.distanceYMaxi=this.center.lat + this.distanceYCalcul;
    this.distanceYMini= this.center.lat - this.distanceYCalcul;
    console.log("la distance calcul est  " + this.distanceXCalcul);
    console.log("la distance Xmaxi" + this.distanceXMaxi);
    console.log("la distanceX mini est" + this.distanceXMini);
    console.log("la distance Ymaxi" + this.distanceYMaxi);
    console.log("la distancey mini est" + this.distanceYMini);
    
  }

  addMarker() {
    this.markers.push({
      position: {
        lat:  this.nearLocations[this.i].y,
        lng: this.nearLocations[this.i].x,
      },
      label: {
        color: 'red',
        text: 'Marker label ' + (this.markers.length + 1),
      },
      title: 'Marker title ' + (this.markers.length + 1),
      info: 'Marker info ' + (this.markers.length + 1),
      options: {
        animation: google.maps.Animation.BOUNCE,
      },
    })
    console.log(this.nearLocations[this.i].x);
    console.log(this.nearLocations[this.i].y);
    this.i ++;
  }

  addNewMarker() {
    this.markers.push({
      position: {
        lat:  this.nearLocations[this.i].y,
        lng: this.nearLocations[this.i].x,
      },
      label: {
        color: 'red',
        text: 'Marker label ' + (this.markers.length + 1),
      },
      title: 'Marker title ' + (this.markers.length + 1),
      info: 'Marker info ' + (this.markers.length + 1),
      options: {
        animation: google.maps.Animation.BOUNCE,
      },
    })
    console.log(this.nearLocations[this.i].x);
    console.log(this.nearLocations[this.i].y);
    this.i ++;
  }

  
  openInfo(marker: MapMarker, content) {
    this.infoContent = content
    this.info.open(marker)
  }

  getNearLocations(){
    
    this.liService.getNearLocations(this.distanceXMaxi,this.distanceXMini,this.distanceYMaxi,this.distanceYMini).subscribe((data) => {
      console.log("je suis la distance x maxi"+ this.distanceXMaxi);
      console.log("je suis la distancex mmini"+ this.distanceXMini);
      console.log("je suis la distancey maxi"+ this.distanceYMaxi);
      console.log("je suis la distance ymini "+ this.distanceYMini);

      this.nearLocations = data;
      
    });
    return this.nearLocations ;
  }

  convertDistanceEnLatLong(){
    console.log(this.center.lat);
    console.log(this.center.lng);
   distanceCalcul : this.distance/(111*Math.cos(this.center.lat));
    this.distanceXMaxi=this.center.lng + this.distanceXCalcul;
    this.distanceXMini= this.center.lng - this.distanceXCalcul;
    console.log("distancemaxi"+ this.distanceXMaxi);
    console.log("distanceMini" + this.distanceXMini);
  }
}