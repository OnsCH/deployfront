import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LieuxAutourComponent } from './lieux-autour.component';
import { LieuxService } from '.././common/service/lieux.service';
import { LieuxObject } from '.././common/data/lieux';


describe('LieuxAutourComponent', () => {
  let component: LieuxAutourComponent;
  let fixture: ComponentFixture<LieuxAutourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LieuxAutourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LieuxAutourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });



  it('should create', () => {
    expect(component).toBeTruthy();
  });
});



describe('CalculatorService simple test', () => {
  let lieuxautourTest: LieuxAutourComponent;
  let lieuxService : LieuxService
  beforeEach(() => {
    lieuxautourTest = new LieuxAutourComponent(lieuxService);
    console.log('creation...');
  });
 
  // tests de add
  describe('getNearLocations', () => {
    it('should return correct answer', () => {
     // const result = lieuxautourTest.getNearLocations();
 const result = lieuxService.getNearLocations(2,0,50,49)
      expect(result).toContain({ x :0.250690199,
      y: 49.022029699,
      localisation: "LA TOUQUES A CHEFFREVILLE-TONNENCOURT 1",
      code_station: "03140070",
      code_cours_eau: null,
      nom_cours_eau: null,
      uri_cours_eau: null,
      nombre_operations: 1,
      geometry: {
          type: "Point",
          crs: {
              type: "name",
              properties: {
                  name: "urn:ogc:def:crs:OGC:1.3:CRS84"
              }
          },
          coordinates: [
              0.2506901987399213,
              49.02202969898312
          ]
      }
    });
    });
  });
 
  // tests de lowerOrZero
  
});

