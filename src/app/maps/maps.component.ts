import { Component, OnInit,ViewChild  } from '@angular/core';
import { MapInfoWindow, MapMarker, GoogleMap } from '@angular/google-maps';
import { couldStartTrivia } from 'typescript';
import { LieuxObject } from '.././common/data/lieux';
import { LieuxService } from '.././common/service/lieux.service';



@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {
  @ViewChild(GoogleMap, { static: false }) map: GoogleMap
  @ViewChild(MapInfoWindow, { static: false }) info: MapInfoWindow
  allLocations: LieuxObject[];
  nearLocations: LieuxObject[];
  newLocations: LieuxObject[];
  local : string;
  occurence : []; 
  topLieux: [];
  i: number;
 distance: number;
 zoom = 12;
  center: google.maps.LatLngLiteral
  options: google.maps.MapOptions = {
    zoomControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    mapTypeId: 'hybrid',
    maxZoom: 1000,
    minZoom: 2,
  }
  markers = [];
  newMarkers = [];
  MyMarker = [];
  infoContent = '';

  constructor(private liService: LieuxService) { }
 
  
  ngOnInit(): void {
    navigator.geolocation.getCurrentPosition((position) => {
    this.center = {
      lat: position.coords.latitude,
      lng: position.coords.longitude,
    }
  });
    this.getAllLocations();
    this.getNewLocations();
    this.i = 0;

  }
  zoomIn() {
    if (this.zoom < this.options.maxZoom) this.zoom++
  }

  zoomOut() {
    if (this.zoom > this.options.minZoom) this.zoom--
  }

  click(event: google.maps.MouseEvent) {
    console.log(event)
  }

  logCenter() {
    navigator.geolocation.getCurrentPosition((position) => {
      this.center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
        
      }
    });

  //  
    this.MyMarker.push({
      icon: {
        url: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
    },
      position: {
      lat:this.center.lat ,
      lng: this.center.lng,
      },
      label: {
        color: 'red',
      },
     
      options: {
        icon: 'http://maps.google.com/mapfiles/marker_green.png'

      },
    })
  
    console.log(JSON.stringify(this.map.getCenter()));
    console.log(this.center.lat);
    console.log(this.center.lng);
  
    
  }
 addallMarkers(){
   
  for(var i = 0;i < this.allLocations.length;i++){
    // Add marker
    this.addMarker();
  }
 }

 adnewlMarkers(){
    // Add marker
    this.addNewMarker();
    this.addMyMarker();
 }


  addMarker() {
    this.markers.push({
      position: {
        lat:  this.allLocations[this.i].y,
        lng: this.allLocations[this.i].x,
      },
      label: {
        color: 'red',
       // text: 'Marker label ' + (this.markers.length + 1),
      },
      title: 'Marker title ' + (this.markers.length + 1),
    //  info: 'Marker info ' + (this.markers.length + 1),
      options: {
      //  animation: google.maps.Animation.BOUNCE,
      icon: 'http://maps.google.com/mapfiles/marker_yellow.png'

      },
    })
    console.log(this.allLocations[this.i].x);
    console.log(this.allLocations[this.i].y);
    this.i ++;
  }
  addNewMarker() {
    this.newMarkers.push({
      icon: {
        url: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
    },
      position: {
        lat:  this.newLocations[this.i].y,
        lng: this.newLocations[this.i].x,
      },
      label: {
        color: 'red',
      //  text: 'Marker label ' + (this.newMarkers.length + 1),
      },
     // title: 'Marker title ' + (this.newMarkers.length + 1),
     // info: 'Marker info ' + (this.newMarkers.length + 1),
      options: {
        //animation: google.maps.Animation.BOUNCE,
        icon: 'http://maps.google.com/mapfiles/marker_green.png'

      },
    })

    
    console.log(this.newLocations[this.i].x);
    console.log(this.newLocations[this.i].y);
    this.i ++;
  }
  
  openInfo(marker: MapMarker, content) {
    this.infoContent = content
    this.info.open(marker)

  }

  
  getAllLocations(){
    this.liService.getAllLocations().subscribe((data) => {
   // var  docs = JSON.parse(data);

      console.log(data);
      this.allLocations = data;
      
    });
  }
   getNewLocations(){
     this.liService.getNewLocations().subscribe((data) => {
      console.log(data);
      this.newLocations= data;
     })
     return this.newLocations;
   }
   
   getoccurence(){
     for (var j=0; j<this.allLocations.length;j++){
    this.local = this.allLocations[j].localisation
     this.liService.getOccurenceLocations(this.local).subscribe((data) => {
     //  console.log(this.local);
       console.log(data);
       this.occurence = data;
     //this.topLieux=this.occurence.sort();
     })
    }
    //console.log(this.occurence);
   // const maxVal = Math.max(...this.occurence)
//console.log(maxVal);
    // const sorted = this.occurence.sort(byValue);
    //console.log(sorted);
    // console.log(sorted[1]);
   }

   getTopLieux(){
    const byValue = (a,b) => a - b;
    const sorted = [...this.occurence].sort(byValue);
    //console.log(sorted);
    console.log(sorted[1]);
   }


   addMyMarker() {
    this.MyMarker.push({
      icon: {
        url: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
    },
      position: {
      lat:this.center.lat ,
      lng: this.center.lng,
      },
      label: {
        color: 'red',
      },
     
      options: {
        icon: 'http://maps.google.com/mapfiles/marker_green.png'

      },
    })
  }
}
