import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { login } from 'app/common/data/login';
import { AuthorizationService } from 'app/common/service/authorization.service';
import { UserService } from 'app/common/service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email:string;
  message : string;
  testLogin : any;
  testLoginName: string;
  testLoginFish: string;
  testLoginPoissonFavoris: string[];
  testLoginEmail: string;
  testLoginLastname : string;
  testLoginNumeroTelephone : string;
  testLoginAddressPostal: string;
  testLoginRole: string;
  testLogindateDeNaissance: string;
  emailVerificationMessage: boolean = false ;
  
    constructor(private userService: UserService,private auth: AuthorizationService,private router: Router) { }
    log : login =
    { 
      id: null,
      firstname: '',
      email: '',
      pass: '',
      message:'',
  
    };
    ngOnInit(): void {
    }
   login(){
      this.userService.findByEmaileUser(this.email).subscribe((data) => {  
   console.log(data);
  this.testLogin = data;
  console.log("testLOginest "+this.testLogin)
  console.log("testlogin name " + data["firstname"] )
  this.testLoginName= data["firstname"];
  this.testLoginFish = data["favoriteFish"];
  this.testLoginNumeroTelephone = data["poissonFavori"];
  this.testLoginAddressPostal = data["addressPostal"];
  this.testLoginRole = data["role"];
  this.testLogindateDeNaissance= data["dateDeNaissance"];
  this.testLoginNumeroTelephone= data["numeroTelephone"];
  this.testLoginLastname= data ["lastname"];
  this.testLoginEmail= data["email"]
  console.log( this.testLoginName);
   if (this.testLogin == null){
  console.log ("retry")
  this.message = "false" ;
   }else
  console.log ("ok");
  this.auth.signIn(this.email,this.testLogin,this.testLoginName,this.testLoginFish,this.testLoginAddressPostal,this.testLoginRole,this.testLoginEmail,this.testLoginLastname,this.testLoginNumeroTelephone,this.testLogindateDeNaissance).subscribe( )
  this.auth.getAuthenticatedUser();
  this.message = "true"
  this.naviguerVersBasicSiLoginOk(this.message);
   }); 
   return this.testLogin ;
   }
  
   
  
   private naviguerVersBasicSiLoginOk(message: string){
    if(message === "true"){
    this.router.navigate(['/']);
  }
     }
  
      }
