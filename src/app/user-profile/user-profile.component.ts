import { Component, OnInit } from '@angular/core';
import { user } from 'app/common/data/user';
import { AuthorizationService } from 'app/common/service/authorization.service';
import { UserService } from 'app/common/service/user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(private userService: UserService,public _auth: AuthorizationService) { }

  ngOnInit() {
  }
  editUser(userr: user) {
    this.userService.setter(userr);
  }
}
