import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormControl, NgForm, Validators} from "@angular/forms";
import { AuthorizationService } from 'app/common/service/authorization.service';
import { UserService } from 'app/common/service/user.service';
import { user } from 'app/common/data/user';
import { HttpService } from "app/common/service/http.service";
import { HttpClient, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';

const headeroption = {
  headers: new HttpHeaders({ 'Access-Control-Allow-Origin':'*','Content-Type': 'application/json' })
};
@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {
 

  testRegister : any;
  confirmCode: boolean = false;
   codeWasConfirmed: boolean = false;
   error: string = "";
  code : string ;
  
    constructor( public http: HttpService ,private auth: AuthorizationService,private empService: UserService, private router: Router) { }
    us: user =
      {
        id: null,
        firstname: '',
        lastname: '',
        numeroTelephone : '',
      addressPostal: '',
      email: '',
      motdePasse:'',
      role: '',
      dateDeNaissance: '',
      username:'',
      poissonFavori: [],
      password:'',
      
      };
      loading = false;
 

  emailFormControl = new FormControl("", [
    Validators.required,
    Validators.email
  ]);

  nameFormControl = new FormControl("", [
    Validators.required,
    Validators.minLength(4)
  ]);
    ngOnInit() {
      this.us = this.empService.getter();
    }
    saveUser(updateEmp: user) {
      if (updateEmp.id === null) {
        this.empService.saveUser(updateEmp).subscribe((data) => {
         console.log("this updatedause "+ JSON.stringify(updateEmp))
          console.log(" this data "+ data);
          this.testRegister = JSON.stringify(data);
          console.log("testregistry est "+ this.testRegister);
          this.auth.register(updateEmp.email,this.testRegister).subscribe();
          this.confirmCode = true;
         this.sendmail(updateEmp.email);
         //this.mailMe();
          //this.router.navigate(['/user-profile']);
          // this.Reset();
        });
      } else {
        this.empService.updateUser(updateEmp).subscribe();
        this.error = "Registration Error has occurred";
  
        // console.log(updateEmp.id);
        this.Reset();
        // this.toastr.success('New Employee Updated Successfully!', 'Employee Updation');
      }
    }
    Reset() {
      this.us = {
        id: null,
        firstname: '',
        lastname: '',
        numeroTelephone : '',
        addressPostal: '',
        email: '',
        motdePasse:'',
        role: '',
        dateDeNaissance: '',
        username: '',
       poissonFavori: [],
       password:'',
      };
  
    }
    validateAuthCode(form: NgForm) {
      const code = form.value.code;
      console.log("this code" + this.code);
      this.auth.confirmAuthCode(this.code).subscribe(
        (data) => {
          this.codeWasConfirmed = true;
          this.confirmCode = false;
        },
        (err) => {
          console.log(err);
          this.error = "Confirm Authorization Error has occurred";
        });
    }
    cancel() {
      this.router.navigate(['/']);
    }

    
    
   sendmail(email : string){
    this.http.sendEmail("http://localhost:3000/sendmail", email).subscribe(
      data => {
        let res:any = data; 
        console.log(
          `👏 > 👏 > 👏 > 👏  is successfully register and mail has been sent and the message id is `
        );
      },
      err => {
        console.log(err);
        this.loading = false;
       
      },() => {
        this.loading = false;
      
      }
    );
  }
   
   


  }

  
  