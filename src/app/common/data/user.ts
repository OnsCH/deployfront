export class user {
    id: number;
    firstname: string;
    lastname: string;
    numeroTelephone : string;
    addressPostal: string;
    email: string;
    motdePasse : string;
    role: string;
    dateDeNaissance: string;
    poissonFavori:string[] ;
    username: string;
    password: string;
}