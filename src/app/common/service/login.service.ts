import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { user } from '../data/user';
import { login } from '../data/login';

const headeroption = {
  headers: new HttpHeaders({ 'Access-Control-Allow-Origin':'*','Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private router: Router) { }
  url = 'https://deployv1-staging.herokuapp.com/User';
  logger : login =
    { 
      id: null,
      firstname: '',
      email: '',
      pass: '',
      message:'',

    }
    findByEmaileUser(email): Observable<user[]> {
      let url = this.url + '/findUser' +'/'+ email;
      return this.http.get<user[]>(url ,headeroption)
     }
     getter() {
      return this.logger;
    }
}


 