import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';


const headeroption = {
  headers: new HttpHeaders({ 'Access-Control-Allow-Origin':'*','Content-Type': 'application/json' })
};
@Injectable({
  providedIn: "root"
})
export class HttpService {
  test = "How r u?";
  constructor(private http: HttpClient) {}

  httpGet(url) {
    return this.http.get(url);
  }

  httpPost(url, {}) {
    return this.http.post(url, { name: "Subrat" });
  }

  sendEmail(url, data) {
    return this.http.post(url, data);
  }
}
