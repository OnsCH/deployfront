import { Injectable } from '@angular/core';
import { ObserveOnMessage } from 'rxjs/internal/operators/observeOn';
import { Observable } from 'rxjs';
import { user } from '../data/user';
import { login } from '../data/login';
import { UserService } from './user.service';
import { HttpClient, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';

const headeroption = {
  headers: new HttpHeaders({ 'Access-Control-Allow-Origin':'*','Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  cognitoUser : any = {"name": "pecheur"}
  cognitoUserFirstName : string ;
  cognitoUserfish : string ;
  cognitoUserlastname :string ;
  cognitoUserRole: string ;
  cognitoUserTelephone :string ;
  cognitouserCodePostale: string ;
  cognitoUserDateNaissance: string;
  cognitoUserEmail : string;
cognitoUserPoissonFavori : any;


  usTest : any;
any : any;
  testfindbymail: user[];
  log : login =
  { 
    id: null,
    firstname: '',
    email: '',
    pass : '',
    message:'',

  }
  us: user =
  {
    id: null,
    firstname: '',
    lastname: '',
    numeroTelephone : '',
  addressPostal: '',
  email: '',
  motdePasse:'',
  role: '',
  dateDeNaissance: '',
  username: '',
  poissonFavori: [],
  password:'',
  };


  constructor(private userService: UserService) { }

  findBymail(mail: string){
    this.userService.findByEmaileUser(mail).subscribe((data) => {  
      console.log("la data de la methode esr "+ data);
     this.testfindbymail = data;
     console.log("le testfindbymailest "+this.testfindbymail)
      }); 
      return this.testfindbymail ;
  }
  register(email, us)  { 
    const attributrList = [];
  return Observable.create(observer => {
     if(email == ""|| us == null  ){
         console.log("register error");
         observer.error("Bad credential");
      } else {
        this.cognitoUser = us;
        console.log("regiser success", this.cognitoUser);
        observer.next(this.cognitoUser);
        observer.complete();
      }
  });
   }

   signIn(email, us:user,name: string,fish:string,addressePostale:string,role:string,emaillogin:string,lastename:string,numeroTelephone: string,dateDeNaissance:string) { 
    return Observable.create(observer => {
      if(email == "" || us == null ){
          console.log("signUp error");
          observer.error("Bad credential");
       } else {
        this.cognitoUser= us;
         this.cognitoUserFirstName = name;
         this.cognitoUserPoissonFavori = name;
         this.cognitoUserfish = fish;
         this.cognitoUserlastname = lastename;
  this.cognitoUserRole = role ;
  this.cognitoUserTelephone = numeroTelephone;
  this.cognitouserCodePostale = addressePostale;
  this.cognitoUserDateNaissance = dateDeNaissance;
  this.cognitoUserEmail = emaillogin;
         console.log(this.cognitoUser);
         console.log("cognitouser first nme " + this.cognitoUserFirstName)
         this.cognitoUser = JSON.stringify(us);
         console.log("signUp success", this.cognitoUser);
         observer.next(this.cognitoUser);
         observer.complete();
       }
   });
    }



   isLoggedIn () {  
     return this.cognitoUser !=null;
   }
   confirmAuthCode(code) { 
    return Observable.create(observer => {
      if ( code != "abcd") {
        console.log("Bad code") ;
        observer.error ("Bad Validation code");
  
      } else {
        console.log("confirmaAuthcode() success") ;
        observer.next("confirme code ok") ;
        observer.complete();
      }
    });
    }
   getAuthenticatedUser ()  {
     return this.cognitoUserFirstName;
   }
   getAuthenticatedUserLasteName ()  {
    return this.cognitoUserlastname;
  }
  getAuthenticatedUserrole()  {
    return this.cognitoUserRole;
  }
  getAuthenticatedUserCodePostale () {
    return this.cognitouserCodePostale;
  }

  getAuthenticatedUseremail() {
    return this.cognitoUserEmail;

  }
  
  getAuthenticatedUseTelephone () {
    return this.cognitoUserTelephone;
  }

  getAuthenticatedUseDateNaissace() {
    return this.cognitoUserDateNaissance;
  }

   getAuthenticateurr ()  {
    return this.cognitoUser;
  }
   logOut ( )  { 
     this.cognitoUser = null;
   }


}