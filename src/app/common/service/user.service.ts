import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { user } from '../data/user';
import { login } from '../data/login';
const headeroption = {
  headers: new HttpHeaders({ 'Access-Control-Allow-Origin':'*','Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private router: Router) { }
  url = 'https://deployv1-staging.herokuapp.com/User';

  us: user =
    {
      id: null,
      firstname: '',
      lastname: '',
      numeroTelephone : '',
    addressPostal: '',
    email: '',
    motdePasse: '',
    role: '',
    dateDeNaissance: '',
    username: '',
   poissonFavori: [],
   password:'',
    };

    log : login =
    { 
      id: null,
      firstname: '',
      email: '',
      pass : '', 
      message:'',

    }
  // Fetching all data from database
  getAllUsers(): Observable<user[]> {
    return this.http.get<user[]>(this.url + '/' + 'getAllUsers', headeroption);
  }
  // Insertion of data
  saveUser(newEmp: user) {
    return this.http.post(this.url + '/' + 'saveUser', newEmp, headeroption);
  }
  // Update Employee
  updateUser(updatedEmp: user): Observable<user> {
    return this.http.put<user>(this.url + '/' + 'updateUser', updatedEmp, headeroption);
  }
  // Delete Employee
  deleteUser(id: number) {
    return this.http.delete(this.url + '/deleteUser/' + id);

  }
  
     editfavoritefish(firstname:string,fishname: string):Observable<user[]>{
      let url = this.url + '/updateUser' +'/'+ firstname;
        return this.http.put<user[]>(url ,fishname,headeroption)
     }

     findbyName(firstname:string):Observable<user[]>{
      let url = this.url + '/findbynameUser' +'/'+ firstname;
      return this.http.get<user[]>(url ,headeroption)
   }

   setterFish(userr: user[],fish :string) {
    userr["favoriteFish"] = fish;
    userr["poissonFavori"] = fish;
  
    this.us.poissonFavori.push(fish);
   // userr["poissonFavori"].push(fish);
    console.log("userr favorite fish est" + userr["favoriteFish"] );
    console.log("userr poisson favoris est" + userr["poissonFavoris"] );

    return userr ;
  }

  dislikeFish(userr: user[]) {
    userr["favoriteFish"] = "";
    console.log("userr dislike fish est" + userr["favoriteFish"] );
    return userr ;
  }
  
  
     findByEmaileUser(email: string): Observable<user[]> {
   let url = this.url + '/findUser' +'/'+ email;
   return this.http.get<user[]>(url ,headeroption)
  }
  setter(userr: user) {
    this.us = userr;
    this.router.navigate(['/register-profile']);
  }
  getter() {
    return this.us;
  }
  
}

