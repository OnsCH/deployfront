import { Injectable, ɵConsole } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';
import { LieuxObject } from '../data/lieux';
import { Observable } from 'rxjs';
import { map , flatMap ,toArray ,filter} from 'rxjs/operators';
import { Router } from '@angular/router';
const headeroption = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class LieuxService {

  constructor(private http: HttpClient, private router: Router) { }
 localisationURL: string; 
  url = 'https://poissondeploynode.herokuapp.com/api/public/localiZation'

  l: LieuxObject =
    {
    //  _id: null,
      x: null,
      y: null,
      localisation : '',
      uri_cours_eau: '',
      code_station: '',
      code_cours_eau: '',
      nom_cours_eau: '',
      nombre_operations: '',
      geometry: null
    };

  getAllLocations(): Observable<LieuxObject[]> {
    return this.http.get<LieuxObject[]>( 'https://poissondeploynode.herokuapp.com/api/public/localiZation', headeroption);
  }
 public getNearLocations(distanceXMaxi: number,distanceXMini: number,distanceYMaxi: number,distanceYMini: number): Observable<LieuxObject[]> {
   let url=`https://poissondeploynode.herokuapp.com/api/public/distance/` + distanceXMaxi +'/'+ distanceXMini + '/'+ distanceYMaxi + '/'+distanceYMini;
   console.log(url);
   
return  this.http.get<LieuxObject[]>(url,headeroption )
}

public getOccurenceLocations(local : string): Observable<[]> {
let url= 'https://poissondeploynode.herokuapp.com/api/public/localiZation/' +local;
//console.log(url);
return this.http.get<[]>(url ,headeroption)
}

public getNewLocations() :Observable<LieuxObject[]> {
  return this.http.get<LieuxObject[]>( 'https://poissondeploynode.herokuapp.com/api/public/newbdd', headeroption);
}

}