import { Component, OnInit } from '@angular/core';
import { user } from 'app/common/data/user';
import { AuthorizationService } from 'app/common/service/authorization.service';
import { UserService } from 'app/common/service/user.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import * as Chartist from 'chartist';
import { content } from 'googleapis/build/src/apis/content';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


   
  closeResult: string;

  constructor(private auth: AuthorizationService,private modalService: NgbModal,private UserService: UserService) { }
 fishlover : string;
 pecheur : user[];
 fish : string = "bar";
  us: user =
  {
    id: null,
    firstname: '',
    lastname: '',
    numeroTelephone : '',
  addressPostal: '',
  email: '',
  motdePasse: '',
  role: '',
  dateDeNaissance: '',
  username:'',
  poissonFavori : [],
  password:'',
  
  };
  ngOnInit(): void {
    this.fishlover=this.auth.cognitoUserFirstName;
    console.log(this.fishlover);
  }
  likeFish(poisson :string) {

    console.log("im try to edit")
     this.UserService.findbyName(this.fishlover).subscribe((data) => {  
      console.log(data);
     this.pecheur = data;
     
    this.UserService.setterFish(this.pecheur,poisson);
      console.log(data);
      console.log (poisson);

  });
  }

 editFavoriteFish(poisson : string){
   this.UserService.editfavoritefish(this.fishlover,poisson).subscribe((data)=>{
     console.log("je uis la methode edit  "+ data);
   })
   this.likeFish(poisson);
 }
  dislikeFish() {
    console.log("im try to edit")
     this.UserService.findbyName(this.fishlover).subscribe((data) => {  
      console.log(data);
     this.pecheur = data;
     
    this.UserService.dislikeFish(this.pecheur);
    console.log("vous dislikez le fish")
      console.log(data);
  });
  }


  saveUser(updateEmp: user) {
    if (updateEmp.id === null) {
      this.UserService.saveUser(updateEmp).subscribe((data) => {
        console.log(data);
         this.Reset();
      });
      // this.toastr.success('New Employee Added Successfully!', 'Employee Registeration');
    } else {
      this.UserService.updateUser(updateEmp).subscribe();
      // console.log(updateEmp.id);
      this.Reset();
      // this.toastr.success('New Employee Updated Successfully!', 'Employee Updation');
    }
  }
  
 openBackDropCustomClass(poisson:string, content) {
    this.modalService.open(content, {backdropClass: 'light-blue-backdrop'});
    this.likeFish(poisson);
    this.editFavoriteFish(poisson);
  }

  openWindowCustomClass(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }

  openSm(content) {
    this.modalService.open(content, { size: 'sm' });
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  openXl(content) {
    this.modalService.open(content, { size: 'xl' });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  openScrollableContent(longContent) {
    this.modalService.open(longContent, { scrollable: true });
  }

  Reset() {
    this.us = {
      id: null,
      firstname: '',
      lastname: '',
      numeroTelephone : '',
      addressPostal: '',
      email: '',
      motdePasse:'',
      role: '',
      dateDeNaissance: '',
      username: '',
      poissonFavori : [],
      password:'',
    };
  }
}
